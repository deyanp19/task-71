import "./App.css";
import Document from "./Document";
import {useState,useEffect} from 'react';


function App() {
  const [text,setText]= useState(null);
  const [err,setError]=useState(null);
  

  const fetchData=()=>{
 
    
  }

  useEffect(() => {
    fetch('https://jaspervdj.be/lorem-markdownum/markdown.txt')
    .then(response=>response.text())
    .then(text=>setText(text))
    .catch((err)=>{setError(err.message); console.log(err);})
  }, [])



  return (
    <div className="App">
      <section className="hero">
        <div className="hero-body">
          <p className="title">A React Task</p>
          <p className="subtitle">by Boom.dev</p>
        </div>
      </section>
      <div className="container is-fullhd">
        <div className="notification">
          Edit the <code>./src</code> folder to add components.
        </div>
        <Document    title="Terms and Conditions" content={text}/>
      </div>
    </div>
  );
}

export default App;
