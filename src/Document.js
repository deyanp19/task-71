import {useState} from 'react';


function Document({title, content}) {
    const [scrolled,setScrolled]=useState('false');
console.log(scrolled);
    const handleScroll = (event) => {
         
        if (event.target.scrollTop + event.target.clientHeight >= event.target.scrollHeight) {
                
           setScrolled(true);
           console.log(scrolled=='true');
        }
        console.log(event.target.scrollTop,event.target.clientHeight,event.target.scrollHeight);
      };

    return(
        <div>
            <h2 className="title">{title}</h2>
            <section onScroll={handleScroll} className="content">{content}</section>
            {scrolled=='false' ? <button disabled >I Agree</button>:  <button   >I Agree</button>}
            
        </div>

    )

}

export default Document;